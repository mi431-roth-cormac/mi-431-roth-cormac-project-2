using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class TreasureTileDungeonGenerator : SimpleRandomWalkDungeonGenerator
{
    [SerializeField]
    private int treasureCount = 5;
    protected override void RunProceduralGeneration()
    {
        HashSet<Vector2Int> floorPositions = RunRandomWalk(randomWalkParameters, startPosition);
        tilemapVisualizer.PaintFloorTiles(floorPositions);
        HashSet<Vector2Int> treasurePositions = RunRandomWalkTreasure(randomWalkParameters, startPosition, floorPositions);
        tilemapVisualizer.PaintTreasureTiles(treasurePositions);
        WallGenerator.CreateWalls(floorPositions, tilemapVisualizer);
    }

    private HashSet<Vector2Int> RunRandomWalkTreasure(SimpleRandomWalkSO parameters, Vector2Int position, HashSet<Vector2Int> floorPositions)
    {
        Vector2Int currentPosition = position;
        HashSet<Vector2Int> treasurePositions = new HashSet<Vector2Int>();
        for (int i = 0; i < treasureCount; )
        {
            HashSet<Vector2Int> path = ProceduralGenerationAlgorithms.SimpleRandomWalk(currentPosition, parameters.walkLength);
            Vector2Int nextTreasure = path.ElementAt(path.Count - 1);

            if(floorPositions.Contains(nextTreasure) && !treasurePositions.Contains(nextTreasure))
            {
                treasurePositions.Add(nextTreasure);
                i++;
            }

            currentPosition = floorPositions.ElementAt(Random.Range(0, floorPositions.Count));
        }

        return treasurePositions;
    }
}
